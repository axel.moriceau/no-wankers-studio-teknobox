# NO WANKERS STUDIO - DAWIN A
## AUTEURS:
- Coralee Pierrot
- Lauranne Apers
- Valentin Morel
- Romain Angier
- Romain Talon
- Axel Moriceau

Le site utilise le Framework Laravel avec Inertia, VueJs et tailwindCSS.
<br>

### Lancer l'application:  

> Prérequis:
<br>

- Php ^7.4
- Laravel ^4.11
- yarn ^1.22.4
- composer ^1.10.10
<br><br>

Mailer:
Installer Mailhog en local pour utiliser les fonctions de mailing puis lancer mailhog depuis le terminal via la commande `mailhog` , ou passer par un autre serveur smtp comme mailtrap, dans ce cas il faudra ajouter la configuration donnée par votre hebergeur smtp dans le fichier `.env`<br><br>

`cp .env.example .env` || use `copy` instead of `cp` on windows<br>
`php artisan:key generate`<br>
`yarn install`<br>
`composer install`<br>
`yarn watch`<br>
`php artisan serve` || ou lancer un serveur web via mamp, valet, xamp...
`php artisan migrate:fresh --seed`
