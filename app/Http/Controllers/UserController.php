<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Command;
use App\Models\Command_Line;

class UserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request,$id)
    {
        
        $commands = DB::select("
        SELECT command.*
        FROM command
        join user on user.id=command.user_id
        WHERE user.id = ?", [Auth()->user()->id]);


        $allcommands = [];
        foreach ($commands as $k => $v) {
            $allcommands[] = new Command();
            $allcommands[$k]->id = $v->id;
            $allcommands[$k]->arts = DB::select("
            SELECT command_line.*
            FROM command_line
            WHERE command_line_id = ?", [$v->id]);;

        }


        return Inertia::render('User/Index', [
            'user' => auth()->user(),
            'commands' => $allcommands
        ]);
    }
}
