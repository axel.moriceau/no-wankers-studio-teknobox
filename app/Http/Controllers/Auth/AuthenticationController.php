<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AuthenticationController extends Controller
{
    public function login(){
        return Inertia::render('Auth/Login');
    }
    public function register(){
        return Inertia::render('Auth/Register');
    }
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('index')->with('success', 'Vous êtes déconnecté');
    }
    public function handleConnection(Request $request){
        $user= (object)[];
        $user->email = $request->get('email');
        $user->password = $request->get('password');
        $authUser = User::query()->firstWhere([
            'password' => md5($user->password),
            'email' => $user->email
        ]);
        if(!$authUser){
            return redirect()->route('index')->with('error', 'Cet utilisateur n\'existe pas');
        }

        Auth::login($authUser);
        return redirect()->route('index');
    }
    public function handleRegistration(Request $request) {
        $user = new User();

        if (!User::query()->where(['email' => $request->get('email')]) || !User::query()->where(['username' => $request->get('un')])) {
            return redirect()->route('login')->with('error', 'Le pseudo ou le mail existe déjà');
        }
        $user->fname = $request->get('fn');
        $user->lname = $request->get('ln');
        $user->uname = $request->get('un');
        $user->addr = $request->get('addr');
        $user->email = $request->get('email');
        $user->pwd = $request->get('password');
        $user->pwdchk = $request->get('confirm');

        if ($user->pwd != $user->pwdchk) return redirect()->route('index')->with('error', 'Validation de mot de passe incorrect');

        DB::table('user')->insert([
            'firstname' => $user->fname,
            'lastname' => $user->lname,
            'email' => $user->email,
            'phone' => null,
            'birthdate' => null,
            'addrFact' => $user->addr,
            'addrSend' => $user->addr,
            'username' => $user->uname,
            'password' => md5($user->pwd),
            'right' => "user",
        ]);


        $this->handleConnection($request);

        return redirect()->route('index')->with('success', 'Inscription validée');

    }
}
