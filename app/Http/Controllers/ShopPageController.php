<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Box;
use App\Models\Article;
use App\Models\Box_Offer;
use Illuminate\Support\Facades\DB;

class ShopPageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //$boxes = DB::table('box')->get();
        //$articles = DB::table('article')->get();
        $boxes = Box::all();
        $articles = Article::all();
        $offers = Box_Offer::all();
        

        /*
        item: {
        title: 'La box arduino',
        desc: 'lorem ipsum dolor sit amet',
        stock: 50,
        price: 29.95,
      },
        */
        $allboxes = [];
        foreach ($boxes as $k => $v) {
            $allboxes[] = new Box_Offer();
            $allboxes[$k]->title = $v->label;
            $allboxes[$k]->desc = $v->description;
            $allboxes[$k]->level = $v->level;
            $allboxes[$k]->stock = 1;
            $allboxes[$k]->price = $v->cost;
        }

        $allarticles = [];
        foreach ($articles as $k => $v) {
            $allarticles[] = new Box_Offer();
            $allarticles[$k]->title = $v->name;
            $allarticles[$k]->desc = $v->description;
            $allarticles[$k]->level = $v->stock;
            $allarticles[$k]->price = $v->cost;
        }


        $alloffers = [];
        foreach ($offers as $k => $v) {
            //echo $v;
            $alloffers[] = new Box_Offer();
            $alloffers[$k]->title = $v->label;
            $alloffers[$k]->badge = $v->badge;
            //$alloffers->content->title = 'lorem title content';
            //$alloffers->content->quantity = 1;
            $alloffers[$k]->content = $v->description;
            $alloffers[$k]->price = $v->cost;
            $alloffers[$k]->delay = ($v->duration == 0) ? null : (($v->duration == 1) ? 'mo' : 'an');
        }
        return Inertia::render('Shop/Index', ['boxes' => $allboxes, "articles" => $articles, 'offers' => $alloffers]);
    }
}
