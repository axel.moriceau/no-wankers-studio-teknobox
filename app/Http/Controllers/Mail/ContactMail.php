<?php

namespace App\Http\Controllers\Mail;

use App\Mail\ContactForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ContactMail extends Controller
{
    public $username;
    public $mail;
    public $body;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username="Axel")
    {
        $this->username = $username;

    }
    public function mail(Request $request)
    {
        $this->mail = $request->get('mail');
        $this->body = $request->get('body');
        $this->subject = $request->get('subject');
       Mail::to(env('MAIL_CONTACT_ADDRESS'))->send(new ContactForm($this->username,$this->mail,$this->subject,$this->body));

       return redirect()->route('index')->with('success', 'Formulaire de contact envoyé');

    }
}
