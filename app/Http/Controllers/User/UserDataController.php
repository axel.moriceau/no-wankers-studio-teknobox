<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserDataController extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->json(['user' => auth()->user()], 200);
    }
}
