<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;
use App\Models\Box;

class HomepageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $box = DB::select('select * from box ORDER BY id DESC LIMIT 0, 1');

        //dd($box);
        $monthbox = new Box();
        $monthbox->title = $box[0]->label;
        $monthbox->desc = $box[0]->description;
        $monthbox->level = ($box[0]->level == 'easy') ? 1 : ($box[0]->level == 'medium' ? 2 : 3);
        $monthbox->pict = $box[0]->pictures; 
        $monthbox->date = $box[0]->date; 

        return Inertia::render('index', ['monthbox' => $monthbox]);
    }
}
