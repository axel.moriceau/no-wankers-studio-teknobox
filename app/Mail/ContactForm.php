<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    public $username;
    public $mail;
    public $body;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username="Axel",$mail="moriceauaxel@gmail.com",$subject="Demande de contact",$body="lorem ipsum dolor sit amet")
    {
        $this->username = $username;
        $this->mail = $mail;
        $this->body = $body;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->replyTo($this->mail, $this->username)
                    ->subject($this->subject)
                    ->from(env('MAIL_FROM_ADDRESS'))
                    ->view('mail.contact.notification');
    }
}
