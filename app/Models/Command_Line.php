<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Command_Line extends Model
{
    use HasFactory;
    protected $table = 'command_line';
    public $timestamps = false;
}
