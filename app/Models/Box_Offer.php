<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Box_Offer extends Model
{
    use HasFactory;
    protected $table = 'box_offer';
    public $timestamps = false;
}
