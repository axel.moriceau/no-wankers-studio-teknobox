<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $addr = $this->faker->address;
        $fname = $this->faker->firstname;
        $lname = $this->faker->lastname;
        return [
            'firstname' => $fname,
            'lastname' => $lname,
            'email' => $this->faker->unique()->email,
            'phone' => $this->faker->phoneNumber,
            'birthdate' => $this->faker->dateTimeBetween('1970-01-01', '2005-12-31')
            ->format('Y-m-d H:i:s'),
            'addrFact' => $addr,
            'addrSend' => $addr,
            'username' => $lname.$fname.rand(0,99),
            'password' => md5(bcrypt($this->faker->password)),
            'right' => 'user',
        ];

    }
}
