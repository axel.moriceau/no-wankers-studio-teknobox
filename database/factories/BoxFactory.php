<?php

namespace Database\Factories;

use App\Models\Box;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BoxFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Box::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $array = array('easy', 'medium', 'hard');

        return [
            'level' => $array[array_rand($array)],
            'description' => $this->faker->realText(),
            'label' => $this->faker->name,
            'date' => date('Y-m-d H:i:s'),
            'cost' => 40,
            'pictures' => "",
        ];

    }
}
