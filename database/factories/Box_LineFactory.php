<?php

namespace Database\Factories;

use App\Models\Box_Line;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class Box_LineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Box_Line::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $boxid = $this->faker->numberBetween(1,4);
        return [
            'box_id' => $boxid,
            'article_id' => $this->faker->unique($boxid)->numberBetween(1,49),
            'count' => rand(1, 5),
        ];

    }
}
