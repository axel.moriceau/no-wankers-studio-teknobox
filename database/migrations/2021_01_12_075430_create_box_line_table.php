<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxLineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('box_line', function (Blueprint $table) {
            //$table->id();
            //$table->foreignId('box_id')->references('id')->on('box');
            $table->foreignId('box_id')->references('id')->on('box');
            $table->foreignId('article_id')->references('id')->on('article');
            $table->integer('count');
            $table->primary(['box_id', 'article_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('box_line');
    }
}
