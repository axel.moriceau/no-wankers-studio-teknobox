<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartLineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_line', function (Blueprint $table) {
            $table->id();
            $table->foreignId('cart_id')->references('id')->on('cart');
            $table->foreignId('article_id')->references('id')->on('article')->nullable();
            $table->foreignId('box_id')->references('id')->on('box')->nullable();
            $table->integer('count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_line');
    }
}
