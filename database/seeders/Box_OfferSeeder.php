<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Box_OfferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('box_offer')->insert([
            'label' => 'Formule 1',
            'badge' => 'Pour les nouveaux venu',
            'description' => 'Si la box du mois vous donne envie, prennez-là dès maintenant et recevez ce mois ci chez vous une teknobox et
            decouvrez nos produits !',
            'cost' => 40,
            'duration' => 0,
        ]);
        DB::table('box_offer')->insert([
            'label' => 'Formule 2',
            'badge' => 'Pour les habitués',
            'description' => 'Vous êtes tentés par nos produits ? Recevez chaque mois chez vous une teknobox.Abonnez-vous aujourd\'hui pour recevoir la Box du mois prochain !',
            'cost' => 35,
            'duration' => 1,
        ]);
        DB::table('box_offer')->insert([
            'label' => 'Formule 3',
            'badge' => 'Pour les vétérans',
            'description' => 'Recevez pendant 1 an chez vous une teknobox tout les mois.
            Abonnez-vous aujourd\'hui pour recevoir la Box du mois prochain ! TECKNOBOX',
            'cost' => 450,
            'duration' => 12,
        ]);
    }
}
