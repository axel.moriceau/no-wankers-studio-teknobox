<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($argv = null)
    {
        if (isset($argv["number"]) && $argv["number"] > 50) { 
            \App\Models\Article::factory($argv["number"])->create(); 
        } else {
            \App\Models\Article::factory(50)->create();
        }
    }
}
