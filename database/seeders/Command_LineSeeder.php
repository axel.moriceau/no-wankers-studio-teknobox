<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class Command_LineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('command_line')->insert([
            'command_line_id' => 1,
            'idart' => 20,
            'cost' => 5,
            'count' => 3
        ]);
        DB::table('command_line')->insert([
            'command_line_id' => 1,
            'idart' => 145,
            'cost' => 50.2,
            'count' => 1
        ]);
        DB::table('command_line')->insert([
            'command_line_id' => 2,
            'idart' => 33,
            'cost' => 0.99,
            'count' => 27
        ]);
    }
}
