<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BoxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($argv = null)
    {
        
        DB::table('box')->insert([
            'level' => "medium",
            'description' => "
                Dans le logiciel mBlock ouvrez un nouveau dossier. N'oubliez pas les étapes :
                    - connecter le câble usb après branchement
                    - connecter > usb
                    - téléverser le microprogramme
                    - cliquez le drapeau vert
            ",
            'label' => "Kit petit bras robotique programmable",
            'date' => date("Y-m-d H:i:s"),
            'cost' => 40,
            'pictures' => "",
        ]);

        DB::table('box')->insert([
            'level' => "medium",
            'description' => "
                Le kit comprend:
                    un moteur pas à pas 
                    une carte arduino uno
                    un capteur à ultrasons 
                    une batterie lipo 7.4v 4000 mAh
                    un contrôleur moteur
                    des roues
                    des pièces en bois pour le châssis du robot 
                    chaîne en plastique et roue dentée 
            ",
            'label' => "Kit robot mobile autonome",
            'date' => date("Y-m-d H:i:s"),
            'cost' => 45,
            'pictures' => "",
        ]);
        
        
        if (isset($argv["number"]) && $argv["number"] > 5) { 
            \App\Models\Box::factory($argv["number"])->create();
        } else {
            \App\Models\Box::factory(5)->create();
        }
    }
}
