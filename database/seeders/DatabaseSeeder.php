<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $nbusers = 4;
        $nbarticles = 100;
        $nbboxes = 10;
        $nbboxe_lines = 10;
        /*
        * preview : call in order all seeder to make good database
        */
        $this->callWith(UserSeeder::class, ['argv' => array('number' => $nbusers)]);
        $this->callWith(ArticleSeeder::class, ['argv' => array('number' => $nbarticles)]);
        $this->callWith(BoxSeeder::class, ['argv' => array('number' => $nbboxes)]);
        $this->callWith(Box_LineSeeder::class, ['argv' => array('number' => $nbboxe_lines)]);
        $this->callWith(Box_OfferSeeder::class);
        $this->callWith(CommandSeeder::class);
        $this->callWith(Command_LineSeeder::class);
    }
}
