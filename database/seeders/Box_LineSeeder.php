<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Box_LineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($argv = null)
    {
        if (isset($argv["number"])) { 
            \App\Models\Box_Line::factory($argv["number"])->create();
        } else {
            \App\Models\Box_Line::factory(20)->create();
        }
    }
}
