<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($argv = null)
    {
        DB::table('user')->insert([
            'firstname' => "admin",
            'lastname' => "istrator",
            'email' => "contact@teknobox.fr",
            'phone' => "05 56 84 57 57",
            'birthdate' => null,
            'addrFact' => "15 Rue de Naudet, 33175 Gradignan",
            'addrSend' => "15 Rue de Naudet, 33175 Gradignan",
            'username' => "admin",
            'password' => md5("teknobox"),
            'right' => 'admin',
        ]);
        DB::table('user')->insert([
            'firstname' => "user",
            'lastname' => "standard",
            'email' => "user@teknobox.fr",
            'phone' => null,
            'birthdate' => null,
            'addrFact' => "15 Rue de Naudet, 33175 Gradignan",
            'addrSend' => "15 Rue de Naudet, 33175 Gradignan",
            'username' => "user",
            'password' => md5("teknobox"),
            'right' => 'user',
        ]);
        
        if (isset($argv["number"])) { 
            \App\Models\User::factory($argv["number"])->create(); 
        } else {
            \App\Models\User::factory(10)->create(); 
        }
    }
}
