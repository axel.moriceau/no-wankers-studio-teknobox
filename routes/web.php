<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Mail\ContactMail;
use App\Http\Controllers\CartPageController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\ShopPageController;
use App\Http\Controllers\AboutUsPageController;
use App\Http\Controllers\Auth\AuthenticationController;
use App\Http\Controllers\BoxsPageController;
use App\Http\Controllers\ContactPageController;

use App\Http\Controllers\User\UserDataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication logic
Route::get('/login', [AuthenticationController::class, 'login'])->name('login');
Route::get('/register', [AuthenticationController::class, 'register'])->name('register');
Route::post('/auth/handle_connection', [AuthenticationController::class, 'handleConnection'])->name('connect');
Route::post('/auth/handle_registration', [AuthenticationController::class, 'handleRegistration'])->name('regist');
Route::get('/logout', [AuthenticationController::class, 'logout'])->name('logout');

// Website pages
Route::get('/', HomepageController::class)->name('index');
Route::get('/shop', ShopPageController::class)->name('shop');
Route::get('/contact', ContactPageController::class)->name('contact');
Route::get('/about_us', AboutUsPageController::class)->name('aboutUs');
Route::get('/cart', CartPageController::class)->name('cart');
Route::get('user/{id}', UserController::class)->name('user');
Route::get('boxs', BoxsPageController::class)->name('boxs');


Route::post('/email/send/contact/form', [ContactMail::class, 'mail'])->name('mail.contact.form');


//getdata for db
Route::get('usr/usr', UserDataController::class)->name('userdata');
