// require('./bootstrap');
import { App, plugin } from '@inertiajs/inertia-vue'
import Vue from 'vue'
import { InertiaProgress as progress } from '@inertiajs/progress/src'

progress.init({
  // The delay after which the progress bar will
  // appear during navigation, in milliseconds.
  delay: 250,
  // The color of the progress bar.
  color: '#33a584',
  // Whether to include the default NProgress styles.
  includeCSS: true,
  // Whether the NProgress spinner will be shown.
  showSpinner: false
})
Vue.config.productionTip = false
Vue.use(plugin)
Vue.mixin({ methods: { route: window.route } })
const el = document.getElementById('app')

new Vue({
  render: (h) =>
    h(App, {
      props: {
        initialPage: JSON.parse(el.dataset.page),
        resolveComponent: (name) => import(`@/Pages/${name}`).then((module) => module.default)
      }
    })
}).$mount(el)
