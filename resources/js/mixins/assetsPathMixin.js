export const assetsPathMixin = {
  data() {
    return {
      assetUrl: window.$ASSET_URL
    }
  },
  methods: {
    assetSrc(name) {
      return `${window.$ASSET_URL}/${name}`
    }
  }
}
