module.exports = {
    purge: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue"
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        fontFamily: {
            'maxwell': ['Maxwell','"Montserrat"', 'sans-serif'],
        },
        extend: {
            colors: {
                'tek': {
                    white: '#FFFFFF',
                    black: '#0C003D',
                    main: '#FF9B6D',
                    title: '#DCFFFD',
                    blue: '#3500D3'

                }
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
